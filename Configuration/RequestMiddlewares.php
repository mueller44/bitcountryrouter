<?php

return [
    'frontend' => [
        'Bitserv/bit-country-add' => [
            'target' => Bitserv\Bitcountryrouter\Routing\Middleware\BitCountryAdd::class,
            'after' => [
                'typo3/cms-frontend/site-resolver'
            ],
            'before' => [
                'typo3/cms-frontend/base-redirect-resolver'
            ],
        ]
    ]
];


