<?php
namespace Bitserv\Bitcountryrouter\Routing\Xclass;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * IMPORTANT:
 *
 * We are not using this XCLASS Version as long as we could use the HOOK in the paralell folder!
 */
class TypoScriptFrontendController extends \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController {


    /**
     * Calculates the cache-hash (or the lock-hash)
     * This hash is unique to the template,
     * the variables ->id, ->type, list of frontend user groups,
     * ->MP (Mount Points) and cHash array
     * Used to get and later store the cached data.
     *
     * @param bool $createLockHashBase Whether to create the lock hash, which doesn't contain the "this->all" (the template information)
     * @return string the serialized hash base
     */
    protected function createHashBase($createLockHashBase = false)
    {
       # echo "creating hash"; print_r($this->pageArguments );exit;
        // Ensure the language base is used for the hash base calculation as well, otherwise TypoScript and page-related rendering
        // is not cached properly as we don't have any language-specific conditions anymore
        $siteBase = $this->getCurrentSiteLanguage() ? (string)$this->getCurrentSiteLanguage()->getBase() : '';

        // Fetch the list of user groups
        /** @var UserAspect $userAspect */
        $userAspect = $this->context->getAspect('frontend.user');

        $hashParameters = [
            'id' => (int)$this->id,
            'type' => (int)$this->type,
            'country' => (int)$_GET['C'],  // stm - this works !
            'gr_list' => (string)implode(',', $userAspect->getGroupIds()),
            'MP' => (string)$this->MP,
            'siteBase' => $siteBase,
            // cHash_array includes dynamic route arguments (if route was resolved)
            'cHash' => $this->cHash_array,
            // additional variation trigger for static routes
            'staticRouteArguments' => $this->pageArguments !== null ? $this->pageArguments->getStaticArguments() : null,
            'domainStartPage' => $this->domainStartPage
        ];
        // Include the template information if we shouldn't create a lock hash
        if (!$createLockHashBase) {
            $hashParameters['all'] = $this->all;
        }
        // Call hook to influence the hash calculation
        $_params = [
            'hashParameters' => &$hashParameters,
            'createLockHashBase' => $createLockHashBase
        ];
        foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['createHashBase'] ?? [] as $_funcRef) {
            GeneralUtility::callUserFunction($_funcRef, $_params, $this);
        }
        return serialize($hashParameters);
    }


}