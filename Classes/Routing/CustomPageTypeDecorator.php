<?php

namespace Bitserv\Bitcountryrouter\Routing;

use TYPO3\CMS\Core\Routing\Enhancer\PageTypeDecorator;
use TYPO3\CMS\Core\Routing\RouteCollection;

/**
 * CustomPageTypeDecorator
 */
class CustomPageTypeDecorator extends PageTypeDecorator
{
    public const IGNORE_INDEX = [
        '/index.html',
        '/index/',
    ];
    public const ROUTE_PATH_DELIMITERS = ['.', '-', '_', '/'];
    /**
     * @param \TYPO3\CMS\Core\Routing\RouteCollection $collection
     * @param array $parameters
     */
    public function decorateForGeneration(RouteCollection $collection, array $parameters): void
    {

        if (is_array($GLOBALS['TYPO3_CONF_VARS']['countries'])){

            $arrCountries = array_flip($GLOBALS['TYPO3_CONF_VARS']['countries']);
            $strLinkingCountry = ($arrCountries[$parameters['C']]) ? $arrCountries[$parameters['C']] : 'xx';
        } else {
            echo "Page could not work without definition of countries in file AdditionalConfiguration!";
        }

      #  parent::decorateForGeneration($collection, $parameters);
      # complete overwriting instead of appending

        $type = isset($parameters['type']) ? (string)$parameters['type'] : null;
        $value = $this->resolveValue($type);
        $considerIndex = $value !== ''
            && in_array($value{0}, static::ROUTE_PATH_DELIMITERS);
        if ($value !== '' && !in_array($value{0}, static::ROUTE_PATH_DELIMITERS)) {
            $value = '/' . $value;
        }

        // analog zu "type"
        $country = isset($parameters['C']) ? (string)$parameters['C'] : null;
        $value = $this->resolveValue($country);
        $considerIndex = $value !== ''
            && in_array($value{0}, static::ROUTE_PATH_DELIMITERS);
        if ($value !== '' && !in_array($value{0}, static::ROUTE_PATH_DELIMITERS)) {
            $value = '/' . $value;
        }


        /**
         * @var string $routeName
         * @var Route $existingRoute
         */

        foreach ($collection->all() as $routeName => $existingRoute) {
            $existingRoutePath = rtrim($existingRoute->getPath(), '/');
            if ($considerIndex && $existingRoutePath === '') {
                $existingRoutePath = $this->index;
            }
            // country
             $existingRoute->setPath($strLinkingCountry.$existingRoutePath . $value);
            #$existingRoute->setPath($existingRoutePath . $value);
            $deflatedParameters = $existingRoute->getOption('deflatedParameters') ?? $parameters;
            if (isset($deflatedParameters['type'])) {
                unset($deflatedParameters['type']);
                $existingRoute->setOption(
                    'deflatedParameters',
                    $deflatedParameters
                );
            }
            // analog zu type - nimmt param (&C=1) am Ende der URL raus !! - endlich gefunden !
            if (isset($deflatedParameters['C'])) {
                unset($deflatedParameters['C']);
                $existingRoute->setOption(
                    'deflatedParameters',
                    $deflatedParameters
                );
            }


        }

        // add trailing slash/

        /**
         * @var string $routeName
         * @var \TYPO3\CMS\Core\Routing\Route $route
         */
        foreach ($collection->all() as $routeName => $route) {
            $path = $route->getPath();
            if (true === \in_array($path, self::IGNORE_INDEX, true)) {
                $route->setPath('/');
            }
        }
    }
}