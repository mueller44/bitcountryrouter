<?php

namespace Bitserv\Bitcountryrouter\Routing\Hook;



/**
 * Class HashBase
 *
 */
class HashBase
{
    /**
     * @param $params
     *
     * @return void
     */
    public function init(&$params)
    {
       // echo "debug: setting custom-cache for GP:C
        $params['hashParameters']['country'] = ['C' => $_GET['C']];
    }
}
