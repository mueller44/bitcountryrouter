<?php
declare(strict_types = 1);
namespace Bitserv\Bitcountryrouter\Routing\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Routing\SiteRouteResult;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Core\Http\Response;

use TYPO3\CMS\Frontend\Controller\ErrorController;

/**
 * Middleware to add a country-segment after the language
 *
 * Example:
 * Request to https://test.com/en/home/
 * will be converted to https://test.com/en/de/home/
 * if a corresponding GP exists
 * it also converts upper case path parts to lower case
 *
 */
class BitCountryAdd implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {

        /*
         * Note: All debug-info will only be dumped in case of "exit"-Statement
         */

        $originalUri = $request->getUri();
        $path = $originalUri->getPath();
        $lowerCasePath = strtolower($path);

        // see also sysext/frontend/middleware/pageArgumentvalidator
        $routeResult = $request->getAttribute('routing', null);

        // step 1 - chop tail of SiteRouteResult
        $tail = $routeResult->getTail();
        #echo "tail:"; var_dump($tail);
        $countryArgument = substr($tail,0,3);
        #echo  $countryArgument; exit;

        if (substr($countryArgument, -1) =="/") {
            // country-argument has a valid char-length (2 digits + slash)
            $validCounryArgument = rtrim($countryArgument, "/");
        }

        #echo "<br>country: ".$validCounryArgument."<br>";
        // $this->getValues = \TYPO3\CMS\Core\Utility\GeneralUtility::_GET(); print_r($this->getValues); exit; //The array is empty at this time !

        // this array (countries) comes form an additionalConfiguration.php File
        if ($validCounryArgument) {
            $queryParams = $request->getQueryParams();
            $queryParams['C'] = $GLOBALS['TYPO3_CONF_VARS']['countries'][$validCounryArgument];
            $request = $request->withQueryParams($queryParams);      
        }

        /*
         *  was, wenn das Land nicht zur Sprache passt, z.B. von aussen eingegeben ?! - auf die 2.Sprache im country2lang-array korrigieren !
         *  im folgenden wird das korrigiert
         */

        if ($routeResult->getLanguage()){
            $currentLanguageUid = $request->getAttribute('language')->getLanguageId();
            #    \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($request->getAttribute('language')->getLanguageId());
            #    echo $languageId; exit;
            if (is_array($GLOBALS['TYPO3_CONF_VARS']['country2lang'])){
                // echo "Language is: ".$currentLanguageUid."<br>";

                # echo "Country is: ".$queryParams['C'];
                 #print_r($GLOBALS['TYPO3_CONF_VARS']['country2lang']);
                if (is_array($GLOBALS['TYPO3_CONF_VARS']['country2lang'][$queryParams['C']])){          
                    if (in_array($currentLanguageUid, $GLOBALS['TYPO3_CONF_VARS']['country2lang'][$queryParams['C']])){
                        // toDo: prüfen, ob english oder fremdsprache aktiv ist ! Sonst wäre z.B. de/de/company möglich
                        // toDo: aktuel geht z.B. /de/de/subpage-1/first-sub/
                        // warum ?! - geprüft: ging auch schon vorher: de/subpage-1 war auch schon möglich
                        // verdacht: es liegt an sys_language_mode = fallback - denn im wapplertemplate ist es nicht so !
                          # echo "alles OK. Sprache ".$currentLanguageUid." is in array. ";
                    } else {
                         # echo "no definition for ".$currentLanguageUid." in country2lang array";
                          // toDo: 404 redirection - initiate action to do 404 later
                        // this will do (but without redirect)
                         return (new Response())->withStatus(404, 'The page  could not be found!');
                    }
                }else {
                    // The Country Segment is missing comletely
                    // this works, but no internal page
                    //return (new Response())->withStatus(404, 'The page  could not be found! Country segment is missing.');

                    // could be used instead of "not found"
                    return new RedirectResponse("/en/xx/please-choose-your-country/", 307);

                   #   echo "Für C-Parameter ".$_GET['C']." ist kein array Definition vorhanden.  "; // $GET['C'] = 0; // reset C param !

                    exit;
                }
            }
        }


        // nothing has changed, nothing to do - jumps out here !
        if ($lowerCasePath === $path) {
            //  return $handler->handle($request);
        }
        $updatedUri = $request->getUri()->withPath($lowerCasePath);

        $doRedirect = false;
        $site = $request->getAttribute('site');
        if ($site instanceof Site) {
            $doRedirect = (bool)$site->getConfiguration()['settings']['redirectOnUpperCase'] ?? false;
        }

        if ($doRedirect) {
            return new RedirectResponse($updatedUri, 307);
        }

        // resolve argument c by given static list and...
        // ...update the path to just work as before, and continue with the process
        $request = $request->withUri($updatedUri);
        $routeResult = $request->getAttribute('routing');
        if ($routeResult instanceof SiteRouteResult) {
          #  echo "!!!<br>".strtolower(str_replace('de/','/',$routeResult->getTail()));

            foreach ($GLOBALS['TYPO3_CONF_VARS']['countries'] as $key=>$val) $arrCountryRemove[] = $key."/";

            // get the custom C-Parameter off from the normal path !
            // echo $routeResult->getTail();
            // shit: the process removes "es/" from the word "responsive-imag"es/" !!! -> produces 404 error !
            //  echo "the url after treatment: <br>".strtolower(str_replace($arrCountryRemove,'',$routeResult->getTail())); exit;
            // toDo: see only the first 3 letters of the tail - do a bit string sparation...

            // get the custom C-Parameter off from the normal path !
//            $routeResult = new SiteRouteResult($updatedUri, $routeResult->getSite(), $routeResult->getLanguage(), strtolower(str_replace($arrCountryRemove,'',$routeResult->getTail())));

            $routeResult = new SiteRouteResult($updatedUri, $routeResult->getSite(), $routeResult->getLanguage(), strtolower(str_replace($arrCountryRemove,'',substr($routeResult->getTail(),0,3))).substr($routeResult->getTail(),3,-1));


            $request = $request->withAttribute('routing', $routeResult);
        }
        return $handler->handle($request);
    }
}
