<?php
defined('TYPO3_MODE') or die();

/**
 * Add custom PageTypeDecorator
 */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['routing']['enhancers']['CustomPageType'] = \Bitserv\Bitcountryrouter\Routing\CustomPageTypeDecorator::class;

// Register custom PageTypeDecorator:
$GLOBALS['TYPO3_CONF_VARS']['SYS']['routing']['enhancers'] += ['CustomPageType' => \Bitserv\Bitcountryrouter\Routing\CustomPageTypeDecorator::class];

/*
// works, but it's xclass ... better hooking - see below
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Frontend\\Controller\\TypoScriptFrontendController'] = array(
    'className' => 'Bitserv\\Bitcountryrouter\\Routing\\Xclass\\TypoScriptFrontendController'
);
*/
// we need to xclass the error handling for GP:C
// att(!): if folder "Xclass" is used, CustomPagetypedecorator gets lost...
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Core\\Error\\PageErrorHandler\\PageContentErrorHandler'] = array(
    'className' => 'Bitserv\\Bitcountryrouter\\Routing\\PageContentErrorHandler'
);


// we need to xclass to avoid double redirection 1. ->/en/ 2. /en/xx/country....
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Frontend\\Middleware\\SiteBaseRedirectResolver'] = array(
    'className' => 'Bitserv\\Bitcountryrouter\\Routing\\SiteBaseRedirectResolver'
);



// Hooking into TSFE for adding C-Parameter into CHASH calculation
call_user_func(function () {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['createHashBase']['bitcountry_hashBaseCustomization'] =
        \Bitserv\Bitcountryrouter\Routing\Hook\HashBase::class . '->init';
});

